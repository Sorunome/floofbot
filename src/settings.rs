use config::{Config, ConfigError, Environment, File};
use serde::Deserialize;
use std::vec::Vec;

#[derive(Deserialize, Debug, Clone)]
pub struct MatrixConfig {
	pub homeserver_url: String,
	pub user_id: String,
	pub access_token: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct JoinMessageConfig {
	pub room_id: String,
	pub message: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct WelcomeConfig {
	pub room_ids: Vec<String>,
	pub message: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct RejectConfig {
	pub message: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Settings {
	pub matrix: MatrixConfig,
	pub join_message: JoinMessageConfig,
	pub main_room_id: String,
	pub command_prefixes: Vec<String>,
	pub welcome: WelcomeConfig,
	pub reject: RejectConfig,
}

impl Settings {
	pub fn load() -> Result<Self, ConfigError> {
		let mut conf = Config::new();

		conf.merge(File::with_name("config.yaml"))?;
		conf.merge(Environment::with_prefix("floofbot").separator("_"))?;
		conf.try_into()
	}
}
