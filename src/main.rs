mod settings;
use crate::settings::Settings;
use matrix_sdk::{
	room,
	ruma::events::{
		room::member::{MemberEventContent, MembershipChange},
		room::message::{MessageEventContent, MessageType, TextMessageEventContent},
		AnyMessageEventContent, SyncMessageEvent, SyncStateEvent,
	},
	ruma::identifiers::{RoomId, UserId},
	Client,
};
use std::convert::TryFrom;
use std::vec::Vec;
use tracing::{debug, error, info};
use tracing_subscriber::EnvFilter;
use url::Url;

struct SyncLoopHandler {
	client: Client,
	config: Settings,
}

struct ParsedCommand {
	command: String,
	args: Vec<String>,
	mentions: Vec<String>,
	power_level: i64,
}

impl ParsedCommand {
	pub fn reference_user_id(
		&self,
		config: &Settings,
	) -> Result<UserId, matrix_sdk::ruma::identifiers::Error> {
		match self
			.mentions
			.iter()
			.find(|v| v != &&config.matrix.user_id)
			.or_else(|| self.args.get(0))
		{
			Some(user_id_str) => UserId::try_from(user_id_str.to_owned()),
			_ => Err(matrix_sdk::ruma::identifiers::Error::InvalidCharacters),
		}
	}
}

fn parse_markdown(message: &str) -> String {
	let mut options = pulldown_cmark::Options::empty();
	options.insert(pulldown_cmark::Options::ENABLE_STRIKETHROUGH);
	let parser = pulldown_cmark::Parser::new_ext(message, options);

	let mut formatted_message: String = String::with_capacity(message.len() * 3 / 2);
	pulldown_cmark::html::push_html(&mut formatted_message, parser);
	formatted_message.trim().to_string()
}

pub async fn send_markdown(
	client: &Client,
	room: &room::Joined,
	user_id: &str,
	raw_message: &str,
) -> matrix_sdk::Result<()> {
	let displayname = match UserId::try_from(user_id.to_owned()) {
		Ok(id) => match room.get_member(&id).await {
			Ok(Some(member)) => member.display_name().unwrap_or(user_id).to_owned(),
			_ => user_id.to_owned(),
		},
		_ => user_id.to_owned(),
	};
	let message = [("USER_ID", user_id), ("DISPLAYNAME", &displayname)]
		.iter()
		.fold(raw_message.to_owned(), |m, (find, replace)| {
			m.replace(find, replace)
		});
	client
		.room_send(
			room.room_id(),
			AnyMessageEventContent::RoomMessage(MessageEventContent::text_html(
				&message, // plain body
				parse_markdown(&message),
			)),
			None,
		)
		.await
		.map(|_| ())
}

impl SyncLoopHandler {
	pub fn new(client: Client, config: Settings) -> Self {
		Self { client, config }
	}

	pub async fn parse_command(
		&self,
		event: &SyncMessageEvent<MessageEventContent>,
	) -> Option<ParsedCommand> {
		let power_level = if let Some(room::Room::Joined(room)) =
			match RoomId::try_from(self.config.main_room_id.to_owned()) {
				Ok(id) => self.client.get_room(&id),
				_ => None,
			} {
			if let Ok(Some(member)) = room.get_member(&event.sender).await {
				member.power_level()
			} else {
				0
			}
		} else {
			0
		};
		let msg = if let SyncMessageEvent {
			content:
				MessageEventContent {
					msgtype: MessageType::Text(TextMessageEventContent { body: msg_str, .. }),
					..
				},
			..
		} = &event
		{
			msg_str
		} else {
			return None;
		};
		let mut args: Vec<String> = msg.split(' ').map(|v| v.to_owned()).collect();
		if args.len() < 2 {
			return None;
		}
		let input_prefix = args.remove(0).to_lowercase();
		let input_command = args.remove(0).to_lowercase();
		for prefix in self.config.command_prefixes.iter() {
			if prefix.to_lowercase() != input_prefix && prefix.to_lowercase() + ":" != input_prefix
			{
				continue;
			}
			// we have a match!
			let mut mentions: Vec<String> = vec![];
			if let SyncMessageEvent {
				content:
					MessageEventContent {
						msgtype:
							MessageType::Text(TextMessageEventContent {
								formatted:
									Some(matrix_sdk::ruma::events::room::message::FormattedBody {
										body: formatted_msg,
										..
									}),
								..
							}),
						..
					},
				..
			} = &event
			{
				if let Ok(dom) = html_parser::Dom::parse(formatted_msg) {
					fn collect_pills(nodes: &[html_parser::Node], mentions: &mut Vec<String>) {
						for node in nodes.iter() {
							if let html_parser::Node::Element(element) = node {
								if element.name.to_lowercase() == "a" {
									// we have an ancor node!
									if let Some(Some(href)) = element.attributes.get("href") {
										let href_lower = href.to_lowercase();
										if href_lower.starts_with("https://matrix.to/#/") {
											if let Ok(identifier) =
												percent_encoding::percent_decode_str(
													href[20..].split('?').next().unwrap_or(""),
												)
												.decode_utf8()
											{
												if identifier.starts_with('@') {
													mentions.push(identifier.to_string());
												}
											}
										}
										if href_lower.starts_with("matrix:user/") {
											if let Ok(identifier) =
												percent_encoding::percent_decode_str(
													href[12..].split('?').next().unwrap_or(""),
												)
												.decode_utf8()
											{
												mentions
													.push("@".to_owned() + &identifier.to_string());
											}
										}
									}
								}
								collect_pills(&element.children, mentions);
							}
						}
					}
					collect_pills(&dom.children, &mut mentions);
				}
			}
			return Some(ParsedCommand {
				command: input_command,
				args,
				mentions,
				power_level,
			});
		}
		None
	}
}

#[matrix_sdk::async_trait]
impl matrix_sdk::EventHandler for SyncLoopHandler {
	async fn on_room_member(&self, room: room::Room, event: &SyncStateEvent<MemberEventContent>) {
		if let room::Room::Joined(room_obj) = room {
			debug!("Got new member event");
			if room_obj.room_id() != &self.config.join_message.room_id {
				return;
			}
			if let MembershipChange::Joined = event.membership_change() {
			} else {
				return;
			}
			info!("Got new join, sending welcome message");

			let res = send_markdown(
				&self.client,
				&room_obj,
				&event.state_key,
				&self.config.join_message.message,
			)
			.await;
			if let Err(e) = res {
				error!("Failed to send message: {}", e);
			}
		}
	}

	async fn on_room_message(
		&self,
		room: room::Room,
		event: &SyncMessageEvent<MessageEventContent>,
	) {
		if let room::Room::Joined(room_obj) = room {
			if let Some(command) = self.parse_command(event).await {
				match command.command.as_str() {
					"welcome" => {
						if command.power_level < 50 {
							return; // insufficient permissions
						}
						if let Ok(welcome_user_id) = command.reference_user_id(&self.config) {
							let res = send_markdown(
								&self.client,
								&room_obj,
								&welcome_user_id.as_str(),
								&self.config.welcome.message,
							)
							.await;
							if let Err(e) = res {
								error!("Failed to send message: {}", e);
							}
							for room_id in self.config.welcome.room_ids.iter() {
								if let Some(room::Room::Joined(room)) =
									match RoomId::try_from(room_id.to_owned()) {
										Ok(id) => self.client.get_room(&id),
										_ => None,
									} {
									let res = room.invite_user_by_id(&welcome_user_id).await;
									if let Err(e) = res {
										error!("Failed to invite user into room: {}", e);
									}
								} else {
									error!("Room to invite not found");
								}
							}
						} else {
							let res = send_markdown(
								&self.client,
								&room_obj,
								&event.sender.as_str(),
								"[DISPLAYNAME](https://matrix.to/#/USER_ID): Please specify whom to welcome!",
							).await;
							if let Err(e) = res {
								error!("Failed to send message: {}", e);
							}
						}
					}
					"teach" => {
						if command.power_level < 50 {
							return; // insufficient permissions
						}
						if let Ok(reject_user_id) = command.reference_user_id(&self.config) {
							let res = send_markdown(
								&self.client,
								&room_obj,
								&reject_user_id.as_str(),
								&self.config.reject.message,
							)
							.await;
							if let Err(e) = res {
								error!("Failed to send message: {}", e);
							}
						} else {
							let res = send_markdown(
								&self.client,
								&room_obj,
								&event.sender.as_str(),
								"[DISPLAYNAME](https://matrix.to/#/USER_ID): Please specify whom to teach!",
							).await;
							if let Err(e) = res {
								error!("Failed to send message: {}", e);
							}
						}
					}
					_ => { /* do nothing */ }
				}
			}
		}
	}
}

#[tokio::main]
async fn main() -> Result<(), matrix_sdk::Error> {
	tracing_subscriber::fmt()
		.with_env_filter(EnvFilter::from_default_env())
		.init();

	info!("Starting up bot...");
	let config = Settings::load().expect("Config file (config.yaml) is not present");

	let homeserver_url =
		Url::parse(&config.matrix.homeserver_url).expect("Couldn't parse the homeserver URL");
	let client = Client::new(homeserver_url).unwrap();

	let session = matrix_sdk::Session {
		access_token: config.matrix.access_token.clone(),
		device_id: "".into(),
		user_id: UserId::try_from(config.matrix.user_id.clone()).unwrap(),
	};

	client.restore_login(session).await?;

	info!("Logged in as {}", config.matrix.user_id);

	client
		.sync_once(matrix_sdk::SyncSettings::default())
		.await?;

	client
		.set_event_handler(Box::new(SyncLoopHandler::new(
			client.clone(),
			config.clone(),
		)))
		.await;

	client.sync(matrix_sdk::SyncSettings::default()).await;

	Ok(())
}
